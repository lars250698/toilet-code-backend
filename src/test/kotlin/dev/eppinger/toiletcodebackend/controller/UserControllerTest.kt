package dev.eppinger.toiletcodebackend.controller

import dev.eppinger.toiletcodebackend.exception.IllegalOperationException
import dev.eppinger.toiletcodebackend.model.UserDTO
import dev.eppinger.toiletcodebackend.service.UserService
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.springframework.mock.web.MockHttpServletResponse

class UserControllerTest {

    @MockK(relaxed = true)
    lateinit var userService: UserService

    @InjectMockKs
    lateinit var userController: UserController

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun getUser() {
        val user = UserDTO(
                id = "testId",
                score = 5
        )
        every { userService.verifyUser("testId:12345") } returns user
        val result = userController.getUser("testId", "testId:12345")
        assertEquals(user, result)
    }

    @Test(expected = IllegalOperationException::class)
    fun getUserRequestingWrongId() {
        val user = UserDTO(
                id = "testId",
                score = 5
        )
        every { userService.verifyUser("testId:12345") } returns user
        userController.getUser("otherTestId", "testId:12345")
    }

    @Test
    fun newUser() {
        val user = UserDTO(
                id = "testId"
        )
        every { userService.createUser() } returns Pair(user, "password")
        val mockResponse = MockHttpServletResponse()
        val result = userController.newUser(true, mockResponse)
        assertEquals("testId;password", result)
        assertEquals("testId;password", mockResponse.getCookie("user")?.value)
    }
}