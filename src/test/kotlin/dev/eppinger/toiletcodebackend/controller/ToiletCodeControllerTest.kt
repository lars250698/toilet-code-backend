package dev.eppinger.toiletcodebackend.controller

import dev.eppinger.toiletcodebackend.exception.InternalException
import dev.eppinger.toiletcodebackend.model.ToiletCodeDTO
import dev.eppinger.toiletcodebackend.model.UserDTO
import dev.eppinger.toiletcodebackend.service.ToiletCodeService
import dev.eppinger.toiletcodebackend.service.UserService
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import kotlin.math.exp

class ToiletCodeControllerTest {

    @MockK(relaxed = true)
    lateinit var toiletCodeService: ToiletCodeService

    @MockK(relaxed = true)
    lateinit var userService: UserService

    @InjectMockKs
    lateinit var toiletCodeController: ToiletCodeController

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun getToiletCodes() {
        val code1 = ToiletCodeDTO(
                name = "test1"
        )
        val code2 = ToiletCodeDTO(
                name = "test2"
        )
        val codes = listOf(code1, code2)
        every { toiletCodeService.getToiletCodesNearPosition(1.0, 2.0, 3.0) } returns codes
        val result = toiletCodeController.getToiletCodes(1.0, 2.0, 3.0)
        assertEquals(codes, result)
    }

    @Test
    fun addToiletCode() {
        val cookie = "testId;123456"
        val user = UserDTO(
                id = "testId"
        )
        val code = ToiletCodeDTO(
                location = GeoJsonPoint(1.0, 2.0),
                name = "McDonalds",
                code = "12345"
        )
        every { userService.verifyUser(cookie) } returns user
        toiletCodeController.addToiletCode(code, cookie)
        verify { toiletCodeService.newToiletCode(code, "testId") }
    }

    @Test(expected = InternalException::class)
    fun addToiletCodeNoUserId() {
        val cookie = "testId;123456"
        val user = UserDTO(
                id = null
        )
        val code = ToiletCodeDTO(
                location = GeoJsonPoint(1.0, 2.0),
                name = "McDonalds",
                code = "12345"
        )
        every { userService.verifyUser(cookie) } returns user
        toiletCodeController.addToiletCode(code, cookie)
    }

    @Test
    fun upvoteToiletCode() {
        val user = UserDTO(
                id = "testId"
        )
        every { userService.verifyUser("testId:12345") } returns user
        toiletCodeController.upvoteToiletCode("testToiletCodeId", "testId:12345")
        verify { toiletCodeService.upvoteToiletCode("testId", "testToiletCodeId") }
    }

    @Test(expected = InternalException::class)
    fun upvoteToiletCodeNoUserId() {
        val user = UserDTO(
                id = null
        )
        every { userService.verifyUser("testId:12345") } returns user
        toiletCodeController.upvoteToiletCode("testToiletCodeId", "testId:12345")
    }

    @Test
    fun downvoteToiletCode() {
        val user = UserDTO(
                id = "testId"
        )
        every { userService.verifyUser("testId:12345") } returns user
        toiletCodeController.downvoteToiletCode("testToiletCodeId", "testId:12345")
        verify { toiletCodeService.downvoteToiletCode("testId", "testToiletCodeId") }
    }

    @Test(expected = InternalException::class)
    fun downvoteToiletCodeNoUserId() {
        val user = UserDTO(
                id = null
        )
        every { userService.verifyUser("testId:12345") } returns user
        toiletCodeController.downvoteToiletCode("testToiletCodeId", "testId:12345")
    }

    @Test
    fun deleteToiletCode() {
        val user = UserDTO(
                id = "testId"
        )
        every { userService.verifyUser("testId:12345") } returns user
        toiletCodeController.deleteToiletCode("testToiletId", "testId:12345")
        verify {toiletCodeService.deleteToiletCode("testToiletId", "testId")}
    }

    @Test(expected = InternalException::class)
    fun deleteToiletCodeNoUserId() {
        val user = UserDTO(
                id = null
        )
        every { userService.verifyUser("testId:12345") } returns user
        toiletCodeController.deleteToiletCode("testToiletId", "testId:12345")
    }
}