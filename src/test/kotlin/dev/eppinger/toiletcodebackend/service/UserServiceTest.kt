package dev.eppinger.toiletcodebackend.service

import dev.eppinger.toiletcodebackend.exception.IllegalOperationException
import dev.eppinger.toiletcodebackend.model.UserDTO
import dev.eppinger.toiletcodebackend.repository.UserRepository
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.apache.commons.codec.digest.DigestUtils
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mindrot.jbcrypt.BCrypt
import java.util.*
import java.util.concurrent.ThreadLocalRandom

class UserServiceTest {

    @MockK(relaxed = true)
    lateinit var userRepository: UserRepository

    @MockK(relaxed = true)
    lateinit var random: ThreadLocalRandom

    @InjectMockKs
    lateinit var userService: UserService

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun getAllUsers() {
        val user1 = UserDTO(
                id = "testId",
                score = 4
        )
        val user2 = UserDTO(
                id = "testId2",
                score = 5
        )
        val userList = listOf(user1, user2)
        every { userRepository.findAll() } returns userList
        val result = userService.getAllUsers()
        assertEquals(userList, result)
    }

    @Test
    fun getAllUsersNoUsers() {
        every { userRepository.findAll() } returns listOf()
        val result = userService.getAllUsers()
        assertEquals(listOf<UserDTO>(), result)
    }

    @Test
    fun createUser() {
        val now = Date()
        val salt = BCrypt.gensalt()
        val pseudoRandom = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

        val pseudoRandomWithTime = pseudoRandom + now.time.toString()
        val expectedSecret = DigestUtils.sha512Hex(pseudoRandomWithTime)
        val hashedExpectedPw = BCrypt.hashpw(expectedSecret, salt)

        val expectedUser = UserDTO(created = now.time, hashedSecret = hashedExpectedPw)
        val expectedResult = Pair(expectedUser, expectedSecret)

        every { userRepository.save(ofType(UserDTO::class)) } returns expectedUser
        every { random.nextInt(0, userService.charPool.size) } returns 0

        val result = userService.createUser(now, salt, pseudoRandom)
        assertEquals(expectedResult, result)
    }

    @Test
    fun deleteUser() {
        val user = UserDTO(
                id = "testUserId",
                score = 5
        )
        val userOptional = Optional.of(user)
        every { userRepository.findById("testId") } returns userOptional
        userService.deleteUser("testId")
        verify { userRepository.delete(user) }
    }

    @Test
    fun incrementScore() {
        val user = UserDTO(
                id = "testUserId",
                score = 5
        )
        val userOptional = Optional.of(user)
        val incremented = user.apply { score++ }
        every { userRepository.findById("testId") } returns userOptional
        every { userRepository.save(incremented) } returns incremented
        val result = userService.incrementScore("testId")
        assertEquals(incremented, result)
        verify { userRepository.save(incremented) }
    }

    @Test
    fun decrementScore() {
        val user = UserDTO(
                id = "testUserId",
                score = 5
        )
        val userOptional = Optional.of(user)
        val decremented = user.apply { score-- }
        every { userRepository.findById("testId") } returns userOptional
        every { userRepository.save(decremented) } returns decremented
        val result = userService.incrementScore("testId")
        assertEquals(decremented, result)
        verify { userRepository.save(decremented) }
    }

    @Test
    fun getUser() {
        val user = UserDTO(
                id = "testUserId",
                score = 5
        )
        val userOptional = Optional.of(user)
        every { userRepository.findById("testId") } returns userOptional
        val result = userService.getUser("testId")
        assertEquals(user, result)
    }

    @Test
    fun verifyUser() {
        val password = "testPassword"
        val hashedPass = BCrypt.hashpw(password, BCrypt.gensalt())
        val id = "testId"
        val cookieval = "$id;$password"
        val user = UserDTO(
                id = "testId",
                score = 5,
                hashedSecret = hashedPass
        )
        val userOptional = Optional.of(user)
        every { userRepository.findById("testId") } returns userOptional
        val result = userService.verifyUser(cookieval)
        assertEquals(user, result)
    }

    @Test
    fun testVerifyUser() {
        val password = "testPassword"
        val hashedPass = BCrypt.hashpw(password, BCrypt.gensalt())
        val id = "testId"
        val user = UserDTO(
                id = "testId",
                score = 5,
                hashedSecret = hashedPass
        )
        val userOptional = Optional.of(user)
        every { userRepository.findById("testId") } returns userOptional
        val result = userService.verifyUser(id, password)
        assertEquals(user, result)
    }

    @Test(expected = IllegalOperationException::class)
    fun testVerifyUserWrongPassowrd() {
        val password = "testPassword"
        val hashedPass = BCrypt.hashpw(password, BCrypt.gensalt())
        val id = "testId"
        val user = UserDTO(
                id = "testId",
                score = 5,
                hashedSecret = hashedPass
        )
        val userOptional = Optional.of(user)
        every { userRepository.findById("testId") } returns userOptional
        userService.verifyUser(id, "wrongPassword")
    }

}
