package dev.eppinger.toiletcodebackend.service

import dev.eppinger.toiletcodebackend.exception.IllegalOperationException
import dev.eppinger.toiletcodebackend.exception.NoEntitiesFoundException
import dev.eppinger.toiletcodebackend.model.ToiletCodeDTO
import dev.eppinger.toiletcodebackend.repository.ToiletCodeRepository
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Test
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.data.geo.Distance
import org.springframework.data.geo.Metrics
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import java.util.*

class ToiletCodeServiceTest {

    @MockK(relaxed = true)
    lateinit var toiletCodeRepository: ToiletCodeRepository

    @MockK(relaxed = true)
    lateinit var userService: UserService

    @InjectMockKs
    lateinit var toiletCodeService: ToiletCodeService

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun getToiletCodesNearPosition() {
        val point = GeoJsonPoint(0.0, 0.0)
        val distance = Distance(1.0, Metrics.KILOMETERS)
        val toiletCodeDTO = ToiletCodeDTO(
                "testToiletCodeId",
                "testSubmittedBy",
                GeoJsonPoint(0.0, 0.0),
                "testName",
                "testCode",
                mutableListOf(),
                mutableListOf(),
                0
        )
        every { toiletCodeRepository.findByLocationNear(point, distance) } returns Optional.of(listOf(toiletCodeDTO))
        val result = toiletCodeService.getToiletCodesNearPosition(0.0, 0.0, 1.0)
        assertEquals(listOf(toiletCodeDTO), result)
    }

    @Test(expected = NoEntitiesFoundException::class)
    fun getToiletCodesNearPositionNoCodes() {
        val point = GeoJsonPoint(0.0, 0.0)
        val distance = Distance(1.0, Metrics.KILOMETERS)
        every { toiletCodeRepository.findByLocationNear(point, distance) } returns Optional.of(listOf())
        toiletCodeService.getToiletCodesNearPosition(0.0, 0.0, 1.0)
    }

    @Test(expected = NoEntitiesFoundException::class)
    fun getToiletCodesNearPositionNull() {
        val point = GeoJsonPoint(0.0, 0.0)
        val distance = Distance(1.0, Metrics.KILOMETERS)
        every { toiletCodeRepository.findByLocationNear(point, distance) } returns Optional.empty()
        toiletCodeService.getToiletCodesNearPosition(0.0, 0.0, 1.0)
    }

    @Test
    fun getToiletCodesForUser() {
        val toiletCodeDTO = ToiletCodeDTO(
                "testToiletCodeId",
                "testSubmittedBy",
                GeoJsonPoint(0.0, 0.0),
                "testName",
                "testCode",
                mutableListOf(),
                mutableListOf(),
                0
        )
        every { toiletCodeRepository.findBySubmittedBy("testUserId") } returns Optional.of(listOf(toiletCodeDTO))
        val result = toiletCodeService.getToiletCodesForUser("testUserId")
        assertEquals(listOf(toiletCodeDTO), result)
    }

    @Test(expected = NoEntitiesFoundException::class)
    fun getToiletCodesForUserNoCodes() {
        every { toiletCodeRepository.findBySubmittedBy("testUserId") } returns Optional.of(listOf())
        toiletCodeService.getToiletCodesForUser("testUserId")
    }

    @Test(expected = NoEntitiesFoundException::class)
    fun getToiletCodesForUserNull() {
        every { toiletCodeRepository.findBySubmittedBy("testUserId") } returns Optional.empty()
        toiletCodeService.getToiletCodesForUser("testUserId")
    }

    @Test
    fun getToiletCode() {
        val toiletCodeDTO = ToiletCodeDTO(
                "testToiletCodeId",
                "testSubmittedBy",
                GeoJsonPoint(0.0, 0.0),
                "testName",
                "testCode",
                mutableListOf(),
                mutableListOf(),
                0
        )
        every { toiletCodeRepository.findById("testToiletCodeId") } returns Optional.of(toiletCodeDTO)
        val result = toiletCodeService.getToiletCode("testToiletCodeId")
        assertEquals(toiletCodeDTO, result)
    }

    @Test(expected = NoEntitiesFoundException::class)
    fun getToiletCodeNoCode() {
        every { toiletCodeRepository.findById("testToiletCodeId") } returns Optional.empty()
        toiletCodeService.getToiletCode("testToiletCodeId")
    }

    @Test
    fun upvoteToiletCode() {
        val toiletCodeDTO = ToiletCodeDTO(
                "testToiletCodeId",
                "testSubmittedBy",
                GeoJsonPoint(0.0, 0.0),
                "testName",
                "testCode",
                mutableListOf(),
                mutableListOf(),
                0
        )
        every { toiletCodeRepository.findById("testToiletCodeId") } returns Optional.of(toiletCodeDTO)
        every { toiletCodeRepository.save(toiletCodeDTO) } returns toiletCodeDTO
        val result = toiletCodeService.upvoteToiletCode("testUserId", "testToiletCodeId")
        assert(result.upvotedBy.contains("testUserId"))
        assertEquals(1, result.score)
    }

    @Test(expected = IllegalOperationException::class)
    fun upvoteToiletCodeAlreadyUpvoted() {
        val toiletCodeDTO = ToiletCodeDTO(
                "testToiletCodeId",
                "testSubmittedBy",
                GeoJsonPoint(0.0, 0.0),
                "testName",
                "testCode",
                mutableListOf("testUserId"),
                mutableListOf(),
                1
        )
        every { toiletCodeRepository.findById("testToiletCodeId") } returns Optional.of(toiletCodeDTO)
        every { toiletCodeRepository.save(toiletCodeDTO) } returns toiletCodeDTO
        toiletCodeService.upvoteToiletCode("testUserId", "testToiletCodeId")
    }

    @Test
    fun upvoteToiletCodeAlreadyDownvoted() {
        val toiletCodeDTO = ToiletCodeDTO(
                "testToiletCodeId",
                "testSubmittedBy",
                GeoJsonPoint(0.0, 0.0),
                "testName",
                "testCode",
                mutableListOf(),
                mutableListOf("testUserId"),
                -1
        )
        every { toiletCodeRepository.findById("testToiletCodeId") } returns Optional.of(toiletCodeDTO)
        every { toiletCodeRepository.save(toiletCodeDTO) } returns toiletCodeDTO
        val result = toiletCodeService.upvoteToiletCode("testUserId", "testToiletCodeId")
        assert(result.upvotedBy.contains("testUserId"))
        assert(!result.downvotedBy.contains("testUserId"))
        assertEquals(1, result.score)
    }

    @Test
    fun downvoteToiletCode() {
        val toiletCodeDTO = ToiletCodeDTO(
                "testToiletCodeId",
                "testSubmittedBy",
                GeoJsonPoint(0.0, 0.0),
                "testName",
                "testCode",
                mutableListOf(),
                mutableListOf(),
                0
        )
        every { toiletCodeRepository.findById("testToiletCodeId") } returns Optional.of(toiletCodeDTO)
        every { toiletCodeRepository.save(toiletCodeDTO) } returns toiletCodeDTO
        val result = toiletCodeService.downvoteToiletCode("testUserId", "testToiletCodeId")
        assert(result.downvotedBy.contains("testUserId"))
        assertEquals(-1, result.score)
    }

    @Test(expected = IllegalOperationException::class)
    fun downvoteToiletCodeAlreadyDownvoted() {
        val toiletCodeDTO = ToiletCodeDTO(
                "testToiletCodeId",
                "testSubmittedBy",
                GeoJsonPoint(0.0, 0.0),
                "testName",
                "testCode",
                mutableListOf(),
                mutableListOf("testUserId"),
                -1
        )
        every { toiletCodeRepository.findById("testToiletCodeId") } returns Optional.of(toiletCodeDTO)
        every { toiletCodeRepository.save(toiletCodeDTO) } returns toiletCodeDTO
        toiletCodeService.downvoteToiletCode("testUserId", "testToiletCodeId")
    }

    @Test
    fun downvoteToiletCodeAlreadyUpvoted() {
        val toiletCodeDTO = ToiletCodeDTO(
                "testToiletCodeId",
                "testSubmittedBy",
                GeoJsonPoint(0.0, 0.0),
                "testName",
                "testCode",
                mutableListOf("testUserId"),
                mutableListOf(),
                1
        )
        every { toiletCodeRepository.findById("testToiletCodeId") } returns Optional.of(toiletCodeDTO)
        every { toiletCodeRepository.save(toiletCodeDTO) } returns toiletCodeDTO
        val result = toiletCodeService.downvoteToiletCode("testUserId", "testToiletCodeId")
        assert(result.downvotedBy.contains("testUserId"))
        assert(!result.upvotedBy.contains("testUserId"))
        assertEquals(-1, result.score)
    }
}
