package dev.eppinger.toiletcodebackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ToiletCodeBackendApplication

fun main(args: Array<String>) {
    runApplication<ToiletCodeBackendApplication>(*args)
}
