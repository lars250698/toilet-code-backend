package dev.eppinger.toiletcodebackend.exception

import java.lang.Exception

class NoEntitiesFoundException: Exception() {
}