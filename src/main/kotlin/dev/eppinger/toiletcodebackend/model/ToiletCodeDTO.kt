package dev.eppinger.toiletcodebackend.model

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.geo.GeoJsonPoint

data class ToiletCodeDTO(
        @Id @JsonProperty("id") val id: String? = null,
        @JsonProperty("submitted_by") val submittedBy: String = "",
        @JsonProperty("location") val location: GeoJsonPoint = GeoJsonPoint(0.0, 0.0),
        @JsonProperty("name") val name: String = "",
        @JsonProperty("code") val code: String = "",
        @JsonProperty("upvoted_by") val upvotedBy: MutableList<String> = mutableListOf(),
        @JsonProperty("downvoted_by") val downvotedBy: MutableList<String> = mutableListOf(),
        @JsonProperty("score") var score: Int = 0
)

