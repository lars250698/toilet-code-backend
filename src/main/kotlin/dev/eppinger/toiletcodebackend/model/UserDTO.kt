package dev.eppinger.toiletcodebackend.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.annotation.Id

data class UserDTO(
        @Id @JsonProperty("id") val id: String? = null,
        @JsonProperty("score") var score: Int = 0,
        @JsonProperty("posts") val posts: MutableList<String>? = mutableListOf(),
        @JsonProperty("created") val created: Long = 0,
        @JsonIgnore val admin: Boolean = false,
        @JsonIgnore val hashedSecret: String = ""
)