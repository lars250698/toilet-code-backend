package dev.eppinger.toiletcodebackend.service

import dev.eppinger.toiletcodebackend.exception.IllegalOperationException
import dev.eppinger.toiletcodebackend.exception.NoEntitiesFoundException
import dev.eppinger.toiletcodebackend.model.UserDTO
import dev.eppinger.toiletcodebackend.repository.UserRepository
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.lang3.RandomStringUtils
import org.mindrot.jbcrypt.BCrypt
import org.springframework.stereotype.Service
import java.security.MessageDigest
import java.util.*

@Service
class UserService(
        val userRepository: UserRepository
) {

    private val randomSize = 30

    val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9') + '_' + '-' + '@' + '$' + '%' + '/' + '(' + ')'

    fun getAllUsers(): List<UserDTO> {
        return userRepository.findAll()
    }

    fun createUser(): Pair<UserDTO, String> {
        return createUser(Date(), BCrypt.gensalt(), generateRandomString(randomSize))
    }

    /**
     * Method with Date as param for testing purposes.
     * Do NOT use this method to create a user.
     * Use the createUser-method without parameters instead.
     * @param now current time
     * @param salt bcrypt salt
     * @param random randomly generated string
     * @return created user
     */
    fun createUser(now: Date, salt: String, random: String): Pair<UserDTO, String> {
        val randomWithTime = random + now.time.toString()
        val secretString = DigestUtils.sha512Hex(randomWithTime)
        val hashedSecret = BCrypt.hashpw(secretString, salt)
        val user = UserDTO(created = now.time, hashedSecret = hashedSecret)
        userRepository.save(user)
        return Pair(user, secretString)
    }

    fun deleteUser(userId: String) {
        val user = getUser(userId)
        userRepository.delete(user)
    }

    fun incrementScore(userId: String): UserDTO {
        val user = getUser(userId)
        user.score++
        userRepository.save(user)
        return user
    }

    fun decrementScore(userId: String): UserDTO {
        val user = getUser(userId)
        user.score--
        userRepository.save(user)
        return user
    }

    fun getUser(userId: String): UserDTO {
        val user = userRepository.findById(userId).orElse(null)
        return user ?: throw NoEntitiesFoundException()
    }

    fun verifyUser(userCookieValue: String): UserDTO {
        val splitCookie = userCookieValue.split(';')
        if (splitCookie.size != 2) {
            throw IllegalOperationException()
        }
        return verifyUser(splitCookie[0], splitCookie[1])
    }

    fun verifyUser(userId: String, secret: String): UserDTO {
        val user = getUser(userId)
        val correctSecret = BCrypt.checkpw(secret, user.hashedSecret)
        if (!correctSecret) {
            throw IllegalOperationException()
        }
        return user
    }

    private fun generateRandomString(length: Int): String {
        return RandomStringUtils.random(length)
    }
}