package dev.eppinger.toiletcodebackend.service

import dev.eppinger.toiletcodebackend.exception.IllegalOperationException
import dev.eppinger.toiletcodebackend.exception.NoEntitiesFoundException
import dev.eppinger.toiletcodebackend.model.ToiletCodeDTO
import dev.eppinger.toiletcodebackend.repository.ToiletCodeRepository
import org.springframework.data.geo.Distance
import org.springframework.data.geo.Metrics
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.stereotype.Service

@Service
class ToiletCodeService(
        val toiletCodeRepository: ToiletCodeRepository,
        val userService: UserService
) {

    fun getToiletCodesNearPosition(lat: Double, lng: Double, distance: Double): List<ToiletCodeDTO> {
        val codes = toiletCodeRepository.findByLocationNear(GeoJsonPoint(lat, lng), Distance(distance, Metrics.KILOMETERS)).orElse(listOf())
        if (codes.isEmpty()) {
            throw NoEntitiesFoundException()
        }
        return codes
    }

    fun getToiletCodesForUser(user: String): List<ToiletCodeDTO> {
        val codes = toiletCodeRepository.findBySubmittedBy(user).orElse(listOf())
        if (codes.isEmpty()) {
            throw NoEntitiesFoundException()
        }
        return codes
    }

    fun getToiletCode(toiletCodeId: String): ToiletCodeDTO {
        val code = toiletCodeRepository.findById(toiletCodeId).orElse(null)
        return code ?: throw NoEntitiesFoundException()
    }

    fun newToiletCode(toiletCodeDTO: ToiletCodeDTO, userId: String) {
        val code = ToiletCodeDTO(
                submittedBy = userId,
                location = toiletCodeDTO.location,
                name = toiletCodeDTO.name,
                code = toiletCodeDTO.code
        )
        toiletCodeRepository.save(code)
    }

    fun deleteToiletCode(toiletCodeId: String, userId: String) {
        val code = getToiletCode(toiletCodeId)
        if (code.submittedBy != userId) {
            throw IllegalOperationException()
        }
        toiletCodeRepository.delete(code)
    }

    fun upvoteToiletCode(userId: String, toiletCodeId: String): ToiletCodeDTO {
        val code = getToiletCode(toiletCodeId)
        if (code.upvotedBy.contains(userId)) {
            throw IllegalOperationException()
        }
        if (code.downvotedBy.remove(userId)) {
            code.score++
            userService.incrementScore(code.submittedBy)
        }
        code.upvotedBy.add(userId)
        code.score++
        userService.incrementScore(code.submittedBy)
        toiletCodeRepository.save(code)
        return code
    }

    fun downvoteToiletCode(userId: String, toiletCodeId: String): ToiletCodeDTO {
        val code = getToiletCode(toiletCodeId)
        if (code.downvotedBy.contains(userId)) {
            throw IllegalOperationException()
        }
        if (code.upvotedBy.remove(userId)) {
            code.score--
            userService.decrementScore(code.submittedBy)
        }
        code.downvotedBy.add(userId)
        code.score--
        userService.decrementScore(code.submittedBy)
        toiletCodeRepository.save(code)
        return code
    }
}