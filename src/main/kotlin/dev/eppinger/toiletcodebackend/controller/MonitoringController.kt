package dev.eppinger.toiletcodebackend.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class MonitoringController {

    @GetMapping("/")
    fun ping(): String {
        return "Success!"
    }
}