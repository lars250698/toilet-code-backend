package dev.eppinger.toiletcodebackend.controller

import dev.eppinger.toiletcodebackend.exception.InternalException
import dev.eppinger.toiletcodebackend.model.ToiletCodeDTO
import dev.eppinger.toiletcodebackend.service.ToiletCodeService
import dev.eppinger.toiletcodebackend.service.UserService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("codes")
class ToiletCodeController(
        val toiletCodeService: ToiletCodeService,
        val userService: UserService
) {

    @GetMapping("/")
    fun getToiletCodes(
            @RequestParam(name = "lat") lat: Double,
            @RequestParam(name = "lng") lng: Double,
            @RequestParam(name = "rad") rad: Double
    ): List<ToiletCodeDTO> {
        return toiletCodeService.getToiletCodesNearPosition(lat, lng, rad)
    }

    @PostMapping("/")
    fun addToiletCode(
            @RequestBody code: ToiletCodeDTO,
            @CookieValue("user") userCookie: String
    ) {
        val user = userService.verifyUser(userCookie)
        toiletCodeService.newToiletCode(code, user.id ?: throw InternalException())
    }

    @GetMapping("/upvote")
    fun upvoteToiletCode(
            @RequestParam(name = "toiletCodeId") toiletCodeId: String,
            @CookieValue("user") userCookie: String
    ) {
        val user = userService.verifyUser(userCookie)
        toiletCodeService.upvoteToiletCode(user.id ?: throw InternalException(), toiletCodeId)
    }

    @GetMapping("/downvote")
    fun downvoteToiletCode(
            @RequestParam(name = "toiletCodeId") toiletCodeId: String,
            @CookieValue("user") userCookie: String
    ) {
        val user = userService.verifyUser(userCookie)
        toiletCodeService.downvoteToiletCode(user.id ?: throw InternalException(), toiletCodeId)
    }

    @DeleteMapping("/")
    fun deleteToiletCode(
          @RequestParam(name = "toiletCodeId") toiletCodeId: String,
          @CookieValue("user") userCookie: String
    ) {
        val user = userService.verifyUser(userCookie)
        toiletCodeService.deleteToiletCode(toiletCodeId, user.id ?: throw InternalException())
    }
}