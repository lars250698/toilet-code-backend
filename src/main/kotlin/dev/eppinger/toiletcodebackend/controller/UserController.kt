package dev.eppinger.toiletcodebackend.controller

import dev.eppinger.toiletcodebackend.exception.IllegalOperationException
import dev.eppinger.toiletcodebackend.model.UserDTO
import dev.eppinger.toiletcodebackend.service.UserService
import org.springframework.web.bind.annotation.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("/user")
class UserController(
        val userService: UserService
) {

    @GetMapping("/{id}")
    fun getUser(
            @PathVariable("id") userId: String,
            @CookieValue("user") userCookie: String
    ): UserDTO {
        val user = userService.verifyUser(userCookie)
        val ownAccount = user.id == userId
        if (!ownAccount) {
            throw IllegalOperationException()
        }
        return user
    }

    @PostMapping("/")
    fun newUser(
            @RequestParam("tos-accepted") tosAccepted: Boolean,
            response: HttpServletResponse
    ): String {
        if (!tosAccepted) {
            throw IllegalOperationException()
        }
        val userInfo = userService.createUser()
        val user = userInfo.first
        val secret = userInfo.second
        val cookieValue = user.id + ";" + secret
        val cookie = Cookie("user", cookieValue)
        response.addCookie(cookie)
        return cookieValue
    }
}