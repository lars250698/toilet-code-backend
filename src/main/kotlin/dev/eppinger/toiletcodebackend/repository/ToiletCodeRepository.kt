package dev.eppinger.toiletcodebackend.repository

import com.mongodb.client.model.geojson.Point
import dev.eppinger.toiletcodebackend.model.ToiletCodeDTO
import org.springframework.data.geo.Distance
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ToiletCodeRepository: MongoRepository<ToiletCodeDTO, String> {
    fun findByLocationNear(p: GeoJsonPoint, d: Distance): Optional<List<ToiletCodeDTO>>
    fun findBySubmittedBy(submittedBy: String): Optional<List<ToiletCodeDTO>>
}