package dev.eppinger.toiletcodebackend.repository

import dev.eppinger.toiletcodebackend.model.UserDTO
import org.springframework.data.mongodb.repository.MongoRepository

interface UserRepository: MongoRepository<UserDTO, String> {

}