# Toilet code backend

## What is toilet code?
Toilet code is a university project. It's an app where you can share access codes 
for toilets in restaurants.

## How to build
Simply clone and build the docker container with `docker build .`